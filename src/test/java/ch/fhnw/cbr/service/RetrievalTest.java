/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.CBR_NS;
import ch.fhnw.cbr.service.data.CaseInstanceVO;
import ch.fhnw.cbr.service.data.CaseMatchVO;
import ch.fhnw.cbr.service.data.CaseViewVO;
import ch.fhnw.cbr.service.data.IndividualVO;
import ch.fhnw.cbr.service.data.LiteralPropertyValueVO;
import ch.fhnw.cbr.service.data.ObjectPropertyInstanceVO;
import ch.fhnw.cbr.service.test.TEST_NS;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sandro.emmenegger
 */
public class RetrievalTest extends AbstractCBRServicesTest {

    @Test
    public void testGetAllIndividuals() {
        CBRServices service = CBRServices.getInstance();
        CaseViewVO caseViewVO = service.findCaseViewByUri(PROJECT_STAFFING_VP);
        List<IndividualVO> allIndividuals = service.getAllIndividuals(caseViewVO.getTypeUri());
        assertNotNull(allIndividuals);
        assertTrue(allIndividuals.size() > 0);
    }

    @Test
    public void testRetreiveCases() {
        CBRServices service = CBRServices.getInstance();
        CaseViewVO caseViewVO = service.findCaseViewByUri(SYSTEM_INTEGRATION_VP);

        CaseInstanceVO querycase = new CaseInstanceVO();
        querycase.setClassUri(CBR_NS.CBR + "UnitTestCase");
        querycase.setLabel("querycase");
        
        
        IndividualVO system = new IndividualVO();
        system.setClassUri(TEST_NS.CBR_UNITTEST + "System");

        List<LiteralPropertyValueVO> systemProps = new ArrayList<>();
        LiteralPropertyValueVO systemLabel = new LiteralPropertyValueVO();
        systemLabel.setTypeUri(CBR_NS.OWL + "AnnotationProperty");
        systemLabel.setUri(CBR.RDFS_LABEL);
        systemLabel.setValue("Oracle");
        systemProps.add(systemLabel);
        
        LiteralPropertyValueVO systemVersion = new LiteralPropertyValueVO();
        systemVersion.setUri(TEST_NS.CBR_UNITTEST + "systemHasVersionName");
        systemVersion.setValue("12g");
        systemProps.add(systemVersion);
        system.setLiteralProperties(systemProps);

        ObjectPropertyInstanceVO caseIsCharacterizedBySystem = new ObjectPropertyInstanceVO();
        caseIsCharacterizedBySystem.setTypeUri(TEST_NS.CBR_UNITTEST + "caseIsCharacterizedBySystem");
        List<IndividualVO> systemInstances = new ArrayList<>();
        systemInstances.add(system);
        caseIsCharacterizedBySystem.setRangeClassInstances(systemInstances);

        List<ObjectPropertyInstanceVO> caseCharacterizedByProperties = new ArrayList();
        caseCharacterizedByProperties.add(caseIsCharacterizedBySystem);
        querycase.setObjectProperties(caseCharacterizedByProperties);

        List<CaseMatchVO> caseMatches = service.retreiveCases(caseViewVO, querycase);
        assertNotNull(caseMatches);
        assertTrue(caseMatches.size() > 0);

        for (CaseMatchVO caseMatchVO : caseMatches) {
            System.out.println("Case: " + caseMatchVO.getCaseName());
            System.out.println("Similarity: " + caseMatchVO.getSimilarity());
            System.out.println(caseMatchVO.getSimLog().toString());
            System.out.println("___________________________________");
            
            if(caseMatchVO.getCaseUri().equals(TEST_CASE_1)){
                assertEquals(0.85d, caseMatchVO.getSimilarity(), 0.01d);
            }
        }
    }

}
