/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service;

import ch.fhnw.cbr.service.data.ConcernVO;
import ch.fhnw.cbr.service.data.CaseViewVO;
import ch.fhnw.cbr.service.data.ClassVO;
import ch.fhnw.cbr.service.data.LiteralPropertyVO;
import ch.fhnw.cbr.service.data.LiteralPropertyValueVO;
import ch.fhnw.cbr.service.data.ObjectPropertyVO;
import ch.fhnw.cbr.service.test.TEST_NS;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test the functions to retrieve the characterisation space 
 * and the similarities applied.
 * 
 * @author sandro.emmenegger
 */
public class CharacterisationTest extends AbstractCBRServicesTest {

    /**
     * Get views resp. viewpoints with addressed concerns and roles.
     */
    @Test
    public void testGetAllCaseViews() {
        CBRServices instance = CBRServices.getInstance();
        List<CaseViewVO> views = instance.getAllCaseViews();
        assertNotNull(views);
        assertTrue(views.size() > 0);
        for (CaseViewVO view : views) {
            assertNotNull(view.getAddressedConcerns());
            assertTrue(view.getAddressedConcerns().size() > 0);
            for (ConcernVO concern : view.getAddressedConcerns()) {
                assertNotNull(concern.getBelongsToRoles());
                assertTrue(concern.getBelongsToRoles().size() > 0);
            }
        }
    }

    /**
     * Base test of reading a characterisation space tree assigned to a view.
     */
    @Test
    public void testGetCaseCharacterisationSpace() {
        ClassVO characterisationVO = getCharacterisation(PROJECT_STAFFING_VP);

        List<ObjectPropertyVO> objectProperties = new ArrayList<>();
        List<LiteralPropertyVO> literalProperties = new ArrayList<>();
        List<LiteralPropertyValueVO> literalPropertiesValues = new ArrayList<>();
        findAllPropertiesAndValues(objectProperties, literalProperties, literalPropertiesValues, characterisationVO);
        assertTrue(objectProperties.size() > 0);
        assertTrue(literalProperties.size() > 0);
        assertTrue(literalPropertiesValues.size() > 0);
    }

    private void findAllPropertiesAndValues(List<ObjectPropertyVO> objectProperties, List<LiteralPropertyVO> literalProperties, List<LiteralPropertyValueVO> literalPropertiesValues, ClassVO characterisationVO) {
        for (ObjectPropertyVO objectProperty : characterisationVO.getObjectProperties()) {
            objectProperties.add(objectProperty);
            findAllPropertiesAndValues(objectProperties, literalProperties, literalPropertiesValues, (ClassVO) objectProperty.getRangeClass());
            List<LiteralPropertyVO> litProps = ((ClassVO) objectProperty.getRangeClass()).getLiteralProperties();
            literalProperties.addAll(litProps);
            for (LiteralPropertyVO litProp : litProps) {
                literalPropertiesValues.addAll(litProp.getAllValues());
            }

        }
    }
    private ClassVO getCharacterisation(String view) {
        CBRServices service = CBRServices.getInstance();
        CaseViewVO caseViewVO = service.findCaseViewByUri(view);
        ClassVO characterisationVO = service.getCaseCharacterisationSpace(caseViewVO);
        assertNotNull(characterisationVO);
        assertNotNull(characterisationVO.getObjectProperties());
        assertTrue(characterisationVO.getObjectProperties().size() > 0);
        return characterisationVO;
    }
}
