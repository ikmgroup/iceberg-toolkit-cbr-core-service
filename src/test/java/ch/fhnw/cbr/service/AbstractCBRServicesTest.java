/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service;

import ch.fhnw.cbr.persistence.OntAO;
import ch.fhnw.cbr.service.test.TEST_NS;
import org.junit.BeforeClass;

/**
 * Base class for CBR services tests.
 * 
 * @author sandro.emmenegger
 */
public abstract class AbstractCBRServicesTest {
    
    //Project staffing view point
    protected static final String PROJECT_STAFFING_VP = TEST_NS.CBR_UNITTEST + "ProjectStaffing_CaseView";
    //Third party system integration viewpoint
    protected static final String SYSTEM_INTEGRATION_VP = TEST_NS.CBR_UNITTEST + "ThirdPartySystemIntegration_CaseView";
    
    protected static final String TEST_CASE_1 = TEST_NS.CBR_UNITTEST + "UnitTestCase_1";
    
    @BeforeClass
    public static void loadOntology() {
        OntAO.getInstance();
        //        OntAO.getInstance().getOntModel().write(System.out, "TTL");
    }
    
}
