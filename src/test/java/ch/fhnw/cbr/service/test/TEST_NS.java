/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service.test;

/**
 * Encapsulates namespaces.
 * @author sandro.emmenegger
 */
public enum TEST_NS {
    CBR_UNITTEST("http://ikm-group.ch/cbr/cbr-unittest#");   
    private final String namespace;
    /**
     * @param text
     */
    private TEST_NS(final String namespace) {
        this.namespace = namespace;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return namespace;
    }
}
