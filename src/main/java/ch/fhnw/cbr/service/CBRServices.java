/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service;

import ch.fhnw.cbr.adaption.AdaptionReasoning;
import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.CBRCase;
import ch.fhnw.cbr.model.CBRCaseView;
import ch.fhnw.cbr.model.sim.CaseMatch;
import ch.fhnw.cbr.persistence.OntAO;
import ch.fhnw.cbr.service.data.CaseInstanceVO;
import ch.fhnw.cbr.service.data.CaseMatchVO;
import ch.fhnw.cbr.service.data.CaseViewVO;
import ch.fhnw.cbr.service.data.ClassVO;
import ch.fhnw.cbr.service.data.IndividualVO;
import ch.fhnw.cbr.service.data.LiteralPropertyVO;
import ch.fhnw.cbr.service.data.LiteralPropertyValueVO;
import ch.fhnw.cbr.service.data.ObjectPropertyInstanceVO;
import ch.fhnw.cbr.service.data.ObjectPropertyVO;
import ch.fhnw.cbr.service.data.mapper.CaseCharacterisationSpaceVOMapper;
import ch.fhnw.cbr.service.data.mapper.CaseInstanceVOMapper;
import ch.fhnw.cbr.service.data.mapper.CaseMatchVOMapper;
import ch.fhnw.cbr.service.data.mapper.CaseViewVOMapper;
import ch.fhnw.cbr.service.data.mapper.IndividualVOMapper;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author sandro.emmenegger
 */
public class CBRServices implements CBRServicesInterface {

    private static final CBRServices SINGELTON = new CBRServices();

    private boolean forceRDFSLabelAndComment = true;

    private CBRServices() {
    }

    public static CBRServices getInstance() {
        return SINGELTON;
    }

    @Override
    public boolean setForceRDFSLabelAndComment(boolean forceRDFSLabelAndComment) {
        this.forceRDFSLabelAndComment = forceRDFSLabelAndComment;
        return this.forceRDFSLabelAndComment;
    }

    @Override
    public List<CaseViewVO> getAllCaseViews() {
        List<CaseViewVO> views = new ArrayList<>();
        for (CBRCaseView cbrView : CBRCaseView.getInstances()) {
            CaseViewVOMapper mapper = new CaseViewVOMapper();
            CaseViewVO view = mapper.createVO(cbrView);
            views.add(view);
        }
        return views;
    }

    @Override
    public CaseViewVO findCaseViewByUri(String uri) {
        CBRCaseView cbrView = CBRCaseView.findCaseViewByURI(uri);
        CaseViewVOMapper mapper = new CaseViewVOMapper();
        CaseViewVO view = mapper.createVO(cbrView);
        return view;
    }

    @Override
    public ClassVO getCaseCharacterisationSpace(CaseViewVO caseView) {
        CBRCaseView cbrView = CBRCaseView.findCaseViewByURI(caseView.getUri());
        CaseCharacterisationSpaceVOMapper mapper = new CaseCharacterisationSpaceVOMapper();
        ClassVO characterisationVO = mapper.createVO(cbrView);
        return characterisationVO;
    }

    @Override
    public List<IndividualVO> getAllIndividuals(String classUri) {
        List<IndividualVO> individualsVO = new ArrayList<>();
        OntClass ontClass = OntAO.getInstance().getOntClass(classUri);
        if (ontClass != null) {
            List<Individual> individuals = OntAO.getInstance().getInstances(ontClass);
            IndividualVOMapper mapper = new IndividualVOMapper();
            for (Individual individual : individuals) {
                IndividualVO individualVO = mapper.createVO(individual);
                individualsVO.add(individualVO);
            }
        }
        return individualsVO;
    }

    @Override
    public CaseInstanceVO getCaseInstance(String instanceUri, CaseViewVO caseView) {

        Individual individual = OntAO.getInstance().getOntModel().getIndividual(instanceUri);
        CaseInstanceVOMapper mapper = new CaseInstanceVOMapper();
        CaseInstanceVO caseInstanceVO = mapper.createVO(individual);
        caseInstanceVO.setClassUri(caseInstanceVO.getTypeUri());
        CaseCharacterisationSpaceVOMapper spaceVOMapper = new CaseCharacterisationSpaceVOMapper();
        CBRCaseView cbrView = CBRCaseView.findCaseViewByURI(caseView.getUri());
        ClassVO ccClass = spaceVOMapper.createVO(cbrView);

        /*if (forceRDFSLabelAndComment) {
            if (queryIndividual.getLiteralProperties() != null) {
                for (LiteralPropertyValueVO queryLitProp : queryIndividual.getLiteralProperties()) {
                    if (queryLitProp.getUri().equals(CBR.RDFS_LABEL) || queryLitProp.getUri().equals(CBR.RDFS_COMMENT)) {
                        merge(individual, queryLitProp);
                    }
                }
            }
        }*/
        //merge literals (NO MULTIVALUE SUPPORT)
        return getCaseInstance(caseInstanceVO, ccClass, individual);
    }

    private CaseInstanceVO getCaseInstance(CaseInstanceVO caseInstanceVO, ClassVO classVO, Individual individual) {
        List<LiteralPropertyValueVO> literalProperties = new ArrayList<>();

        if (classVO.getLiteralProperties() != null) {
            for (LiteralPropertyVO ccLitProp : classVO.getLiteralProperties()) {
                if (individual.getPropertyValue(OntAO.getInstance().getOntModel().getProperty(ccLitProp.getUri())) != null) {
                    LiteralPropertyValueVO propertyValueVO = new LiteralPropertyValueVO();
                    propertyValueVO.setValue(individual.getPropertyValue(OntAO.getInstance().getOntModel().getProperty(ccLitProp.getUri())).toString());
                    if (propertyValueVO.getValue().contains("@en") || propertyValueVO.getValue().contains("#string")) //todo: there must be a bug in "CaseCharacterisationSpaceVOMapper"
                    {
                        break;
                    }
                    propertyValueVO.setUri(ccLitProp.getUri());
                    literalProperties.add(propertyValueVO);
                }
            }
        }

        List<ObjectPropertyInstanceVO> objectProperties = new ArrayList<>();

        if (classVO.getObjectProperties() != null) {
            for (ObjectPropertyVO ccObjectProp : classVO.getObjectProperties()) {
                List<IndividualVO> individualVOs = new ArrayList<>();
                ObjectPropertyInstanceVO propertyValueVO = new ObjectPropertyInstanceVO();
                propertyValueVO.setTypeUri(ccObjectProp.getUri());
                List<RDFNode> queryCaseRefIstances = individual.listPropertyValues(OntAO.getInstance().getOntProperty(ccObjectProp.getUri())).toList();
                for (RDFNode node : queryCaseRefIstances) {
                    if (node instanceof OntResource) {
                        if (((OntResource) node).isIndividual()) {
                            Individual individualLocal = ((OntResource) node).asIndividual();
                            CaseInstanceVO caseInstanceVOLocal = new CaseInstanceVO();
                            caseInstanceVOLocal.setClassUri(ccObjectProp.getRangeClass().getUri());
                            caseInstanceVOLocal.setUri(individualLocal.getURI());
                            String label = individualLocal.getLabel(CBR.LANGUAGE);
                            label = label == null ? individualLocal.getLabel(null) : label;
                            label = label == null ? caseInstanceVOLocal.getUri() : label;
                            label = label.contains("#") ? label.substring(label.indexOf("#") + 1) : label;
                            caseInstanceVOLocal.setLabel(label);
                            individualVOs.add(getCaseInstance(caseInstanceVOLocal, ccObjectProp.getRangeClass(), individualLocal));
                        }
                    }
                }
                propertyValueVO.setRangeClassInstances(individualVOs);
                objectProperties.add(propertyValueVO);
            }
        }
        caseInstanceVO.setLiteralProperties(literalProperties);
        caseInstanceVO.setObjectProperties(objectProperties);
        return caseInstanceVO;
    }

    @Override
    public CaseInstanceVO createOrUpdateCase(CaseViewVO caseView, CaseInstanceVO caseInstanceVO) {

        assert caseInstanceVO != null;
        assert caseInstanceVO.getUri() != null || (caseInstanceVO.getUri() == null && caseInstanceVO.getClassUri() != null && caseInstanceVO.getLabel() != null);

        CBRCaseView cbrView = CBRCaseView.findCaseViewByURI(caseView.getUri());
        CBRCase resource = new CBRCase(cbrView);
        if (caseInstanceVO.getUri() == null) {
            Individual newInstance = resource.createNewInstance(generateNewIndividualUri(caseInstanceVO.getClassUri()), caseInstanceVO.getLabel());
            caseInstanceVO.setUri(newInstance.getURI());
        } else {
            Individual existingInstance = OntAO.getInstance().getOntModel().getIndividual(caseInstanceVO.getUri());
            if (existingInstance == null) {
                String label = caseInstanceVO.getLabel() != null ? caseInstanceVO.getLabel() : caseInstanceVO.getUri();
                if (caseInstanceVO.getLabel() == null && label.contains("#")) {
                    label = label.substring(label.indexOf("#") + 1);
                }
                Individual newInstance = resource.createNewInstance(caseInstanceVO.getUri(), label);
                caseInstanceVO.setUri(newInstance.getURI());
                caseInstanceVO.setLabel(label);
            }
        }

        CaseCharacterisationSpaceVOMapper mapper = new CaseCharacterisationSpaceVOMapper();
        ClassVO caseCharacterisation = mapper.createVO(cbrView);
        merge(caseCharacterisation, caseInstanceVO);
        return caseInstanceVO;
    }

    private String generateNewIndividualUri(String classUri) {
        String localClassName = classUri.contains("#") ? classUri.substring(classUri.indexOf("#") + 1) : "";
        return CBR.CONFIG.getString("data.namespace") + localClassName + "_" + UUID.randomUUID().toString();
    }

    private Individual merge(ClassVO ccClass, IndividualVO queryIndividual) {
        assert ccClass != null;
        assert queryIndividual != null;
        assert queryIndividual.getUri() != null || queryIndividual.getClassUri() != null;

        Individual individual;
        if (queryIndividual.getUri() == null) {
            String individualUri = generateNewIndividualUri(queryIndividual.getClassUri());
            OntClass ontClass = OntAO.getInstance().getOntClass(queryIndividual.getClassUri());
            individual = OntAO.getInstance().getOntModel().createIndividual(individualUri, ontClass);
            queryIndividual.setUri(individualUri);
        } else {
            individual = OntAO.getInstance().getOntModel().getIndividual(queryIndividual.getUri());
        }

        if (forceRDFSLabelAndComment) {
            if (queryIndividual.getLiteralProperties() != null) {
                for (LiteralPropertyValueVO queryLitProp : queryIndividual.getLiteralProperties()) {
                    if (queryLitProp.getUri().equals(CBR.RDFS_LABEL) || queryLitProp.getUri().equals(CBR.RDFS_COMMENT)) {
                        merge(individual, queryLitProp);
                    }
                }
            }
        }

        //merge literals (NO MULTIVALUE SUPPORT)
        if (ccClass.getLiteralProperties() != null) {
            for (LiteralPropertyVO ccLitProp : ccClass.getLiteralProperties()) {

                LiteralPropertyVO toBeRemoved = ccLitProp;
                if (queryIndividual.getLiteralProperties() != null) {
                    for (LiteralPropertyValueVO queryLitProp : queryIndividual.getLiteralProperties()) {
                        if (ccLitProp.getUri().equals(queryLitProp.getUri())) {
                            toBeRemoved = null;
                            merge(individual, queryLitProp);
                        }
                    }
                }
                if (toBeRemoved != null) {
                    OntProperty literalProperty = OntAO.getInstance().getOntModel().getOntProperty(toBeRemoved.getUri());
                    if (literalProperty != null) {
                        individual.removeAll(literalProperty);
                    }
                }
            }
        }

        //merge object properties
        if (ccClass.getObjectProperties() != null) {
            for (ObjectPropertyVO ccObjectProp : ccClass.getObjectProperties()) {

                boolean exists = false;
                if (queryIndividual.getObjectProperties() != null) {

                    //find properties of query matching the characterisation property
                    for (ObjectPropertyInstanceVO queryObjectProperty : queryIndividual.getObjectProperties()) {
                        if (ccObjectProp.getUri().equals(queryObjectProperty.getTypeUri())) {
                            exists = true;
                            merge(individual, ccObjectProp, queryObjectProperty);
                            break;
                        }
                    }
                }

                if (!exists) {
                    //remove property
                    remove(individual, ccObjectProp);
                }
            }
        }

        return individual;
    }

    /**
     * Either updates, creates new or removes referenced instances.
     *
     * @param individual
     * @param ccObjectProperty
     * @param queryObjectProperty
     * @return
     */
    private void merge(Individual individual, ObjectPropertyVO ccObjectProperty, ObjectPropertyInstanceVO queryObjectProperty) {

        OntProperty property = OntAO.getInstance().getOntProperty(ccObjectProperty.getUri());

        NodeIterator individualsitr = individual.listPropertyValues(property);
        Map<String, Individual> toBeRemoved = new HashMap<>();
        while (individualsitr.hasNext()) {
            RDFNode node = individualsitr.next();
            if (node instanceof OntResource) {
                if (((OntResource) node).isIndividual()) {
                    toBeRemoved.put(individual.getURI(), individual);
                }
            }
        }

        for (IndividualVO referencedInstance : queryObjectProperty.getRangeClassInstances()) {
            //create or update instance
            Individual refIndividual = merge(ccObjectProperty.getRangeClass(), referencedInstance);

            if (toBeRemoved.containsKey(refIndividual.getURI())) {
                //remove updated from removal list
                toBeRemoved.remove(referencedInstance.getUri());
            } else {
                //add property to reference the new instance
                individual.addProperty(property, refIndividual);
            }
        }

        //remove instances if not restricted
        for (Map.Entry<String, Individual> entry : toBeRemoved.entrySet()) {
            //remove property and value (instance)
            //remove(individual, ccObjectProperty, entry.getValue()); // todo: removal needs to be fixed!!!!
        }
    }

    /**
     * Either updates, removes or creates literal property value of query.
     *
     * @param individual
     * @param queryLiteral
     */
    private void merge(Individual individual, LiteralPropertyValueVO queryLiteral) {

        Property property = OntAO.getInstance().getOntModel().getProperty(queryLiteral.getUri());

        RDFNode propertyNode = individual.getPropertyValue(property);
        boolean newOrUpdated = false;
        if (propertyNode != null && propertyNode.isLiteral()) {
            String value = propertyNode.asLiteral().getString();
            if (!queryLiteral.getValue().equals(value)) {
                individual.removeProperty(property, propertyNode);
                newOrUpdated = true;
            }
        } else {
            newOrUpdated = true;
        }

        if (newOrUpdated) {
            switch (property.getURI()) {
                case CBR.RDFS_LABEL:
                    individual.addLabel(queryLiteral.getValue(), CBR.LANGUAGE);
                    break;
                case CBR.RDFS_COMMENT:
                    individual.addComment(queryLiteral.getValue(), CBR.LANGUAGE);
                    break;
                default:
                    Literal newLiteral = OntAO.getInstance().getOntModel().createLiteral(queryLiteral.getValue());
                    individual.addProperty(property, newLiteral);
            }
        }
    }

    // Inner class for the remove operation avoiding ConcurrentModificationException
    private class RemovableTriple {

        public Individual subject;
        public ObjectPropertyVO ccObjectProperty;
        public Individual object;

        public RemovableTriple(Individual subject, ObjectPropertyVO ccObjectProperty, Individual object) {
            this.subject = subject;
            this.ccObjectProperty = ccObjectProperty;
            this.object = object;
        }
    }

    /**
     * Removes all instances in property range considering the cascading rule.
     *
     * @param individual
     * @param ccObjectProperty
     */
    private void remove(Individual individual, ObjectPropertyVO ccObjectProperty) {
        //Avoiding ConcurrentModificationException
        List<RemovableTriple> triplesToRemove = new ArrayList<RemovableTriple>();
        OntProperty rangeClassObjectProperty = OntAO.getInstance().getOntProperty(ccObjectProperty.getUri());
        NodeIterator individualsitr = individual.listPropertyValues(rangeClassObjectProperty);
        while (individualsitr.hasNext()) {
            RDFNode node = individualsitr.next();
            if (node instanceof OntResource) {
                OntResource ontResource = ((OntResource) node);
                if (ontResource.isIndividual()) {
                    triplesToRemove.add(new RemovableTriple(individual, ccObjectProperty, ontResource.asIndividual()));
                }
            }
        }

        for (RemovableTriple removableTriple : triplesToRemove) {
            remove(removableTriple.subject, removableTriple.ccObjectProperty, removableTriple.object);
        }
    }

    /**
     * Deep removal of a linked instance. Removes first the statement (subject,
     * property, object) resp. the edge. Based on the cascading rule defined for
     * the characterisation property, the referenced instance is deleted as well
     * (deep recursive removal).
     *
     * @param subject
     * @param ccObjectProperty
     * @param object
     */
    private void remove(Individual subject, ObjectPropertyVO ccObjectProperty, Individual object) {
        //remove property
        OntProperty property = OntAO.getInstance().getOntProperty(ccObjectProperty.getUri());
        OntAO.getInstance().getOntModel().remove(subject, property, object);

        switch (ccObjectProperty.getCascadeDeleteRule()) {
            case NEVER:
                return;
            case RESTRICTED: {
                //TODO check if other instance is referenced by others and return if so -> restric removal.
            }
        }

        //deep remove of range instance (object)
        List<ObjectPropertyVO> ccRangeClassObjectProperties = ccObjectProperty.getRangeClass().getObjectProperties();
        if (ccRangeClassObjectProperties != null) {
            for (ObjectPropertyVO ccRangeClassObjectProperty : ccRangeClassObjectProperties) {
                remove(object, ccRangeClassObjectProperty);
            }
        }
        object.remove();

    }

    @Override
    public List<CaseMatchVO> retreiveCases(CaseViewVO caseView, CaseInstanceVO querycase) {
        CBRCaseView cbrView = CBRCaseView.findCaseViewByURI(caseView.getUri());
        List<CaseMatchVO> caseMatchesVO = new ArrayList<>();
        createOrUpdateCase(caseView, querycase);
        Individual individual = OntAO.getInstance().getOntModel().getIndividual(querycase.getUri());
        if (individual != null) {
            List<CaseMatch> caseMatches = cbrView.retreive(individual);
            CaseMatchVOMapper mapper = new CaseMatchVOMapper();
            for (CaseMatch caseMatch : caseMatches) {
                CaseMatchVO caseMatchVO = mapper.createVO(caseMatch);
                CaseInstanceVO caseInstanceVO = getCaseInstance(caseMatch.getCaseURI(), caseView);
                caseMatchVO.setCaseInstanceVO(caseInstanceVO);
                caseMatchesVO.add(caseMatchVO);
            }
        }
        return caseMatchesVO;

    }

    @Override
    public void adaptCase(String newCaseInstanceUri, String selectedCaseInstanceUri) {
        AdaptionReasoning adaptionReasoning = new AdaptionReasoning();
        adaptionReasoning.applyRulesAndAdd(newCaseInstanceUri, selectedCaseInstanceUri);
    }

    @Override
    public CaseInstanceVO getCaseFileItems(String caseInstanceUri) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateCaseFileItems(String caseInstanceUri, List caseFileItems) {
        throw new UnsupportedOperationException();
    }

}
