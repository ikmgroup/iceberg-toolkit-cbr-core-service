/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service.data;

/**
 *
 * @author sandro.emmenegger
 */
public class CaseMatchVO{
    
    private CaseInstanceVO caseInstanceVO;
    private Double similarity;
    
    private String caseName;
    private String caseUri;
    private String simLog;
    private String caseStateUri;
    private String caseStateLabel;

    public CaseInstanceVO getCaseInstanceVO() {
        return caseInstanceVO;
    }

    public void setCaseInstanceVO(CaseInstanceVO caseInstanceVO) {
        this.caseInstanceVO = caseInstanceVO;
    }

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getCaseUri() {
        return caseUri;
    }

    public void setCaseUri(String caseUri) {
        this.caseUri = caseUri;
    }

    public String getSimLog() {
        return simLog;
    }

    public void setSimLog(String simLog) {
        this.simLog = simLog;
    }

    public String getCaseStateUri() {
        return caseStateUri;
    }

    public void setCaseStateUri(String caseStateUri) {
        this.caseStateUri = caseStateUri;
    }

    public String getCaseStateLabel() {
        return caseStateLabel;
    }

    public void setCaseStateLabel(String caseStateLabel) {
        this.caseStateLabel = caseStateLabel;
    }
    
    

}
