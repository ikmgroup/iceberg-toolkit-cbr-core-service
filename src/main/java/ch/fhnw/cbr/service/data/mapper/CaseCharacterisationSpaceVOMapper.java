/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service.data.mapper;

import ch.fhnw.cbr.model.CBRCase;
import ch.fhnw.cbr.model.CBRCaseView;
import ch.fhnw.cbr.model.CCAnnotationProperty;
import ch.fhnw.cbr.model.CCLiteralProperty;
import ch.fhnw.cbr.model.CCNode;
import ch.fhnw.cbr.model.CCOntClass;
import ch.fhnw.cbr.model.CCRefProperty;
import ch.fhnw.cbr.persistence.OntAO;
import ch.fhnw.cbr.service.data.ClassVO;
import ch.fhnw.cbr.service.data.LiteralPropertyVO;
import ch.fhnw.cbr.service.data.LiteralPropertyValueVO;
import ch.fhnw.cbr.service.data.ObjectPropertyVO;
import ch.fhnw.cbr.service.data.SimilarityAnnotationVO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sandro.emmenegger
 */
public class CaseCharacterisationSpaceVOMapper extends AbstractVOMapper<CBRCaseView, ClassVO> {

    @Override
    public ClassVO createVO(CBRCaseView cbrCaseView) {
        CBRCase cbrCase = new CBRCase(cbrCaseView);
        ClassVO characterisationVO = handle(cbrCase);
        return characterisationVO;
    }

    private ObjectPropertyVO handle(CCRefProperty refProperty) {
        ObjectPropertyVO propertyVO = new ObjectPropertyVO();
        initVO(propertyVO, refProperty);
        propertyVO.setSimilarityAnnotation(createAnnotationSimilarityVO(refProperty));
        propertyVO.setRangeClass(handle(refProperty.getRefResource()));
        return propertyVO;
    }

    private ClassVO handle(CCOntClass ontClass) {
        ClassVO classVO = new ClassVO();
        initVO(classVO, ontClass);
        classVO.setSimilarityAnnotation(createAnnotationSimilarityVO(ontClass));
        for (CCNode ccNode : ontClass.getProperties()) {
            if (ccNode instanceof CCLiteralProperty) {
                classVO.addProperty(handle(ontClass, (CCLiteralProperty) ccNode));
            }
            if (ccNode instanceof CCRefProperty) {
                classVO.addProperty(handle((CCRefProperty) ccNode));
            }
        }
        return classVO;
    }

    private LiteralPropertyVO handle(CCOntClass ontClass, CCLiteralProperty litProperty) {
        LiteralPropertyVO propertyVO = new LiteralPropertyVO();
        initVO(propertyVO, litProperty);
        propertyVO.setSimilarityAnnotation(createAnnotationSimilarityVO(litProperty));
        List<LiteralPropertyValueVO> allValues = new ArrayList<>();
        
        //add existing values
        for (Individual instance : ((CCOntClass) ontClass).getInstances()) {
            String value = "";

            if (litProperty instanceof CCAnnotationProperty) {
                value = ((CCAnnotationProperty) litProperty).getValue(instance);
                value = value == null ? "" : value;
            }
            if (value.isEmpty()) {
                OntProperty ontProperty = OntAO.getInstance().getOntModel().getOntProperty(litProperty.getURI());
                List<RDFNode> properties = instance.listPropertyValues(ontProperty).toList();

                //consider only one value
                if (properties.size() > 0) {
                    RDFNode rdfNode = properties.get(0);
                    if (rdfNode.isLiteral()) {
                        Literal literal = rdfNode.asLiteral();
                        Object objValue = literal != null ? literal.getValue() : "";
                        value = objValue != null ? objValue.toString() : "";
                    }
                }
            }
            LiteralPropertyValueVO literalPropertyValueVO = new LiteralPropertyValueVO();
            literalPropertyValueVO.setValue(value);
            allValues.add(literalPropertyValueVO);
        }
        propertyVO.setAllValues(allValues);
        return propertyVO;
    }

    private SimilarityAnnotationVO createAnnotationSimilarityVO(CCNode ccNode) {
        SimilarityAnnotationVO simVO = new SimilarityAnnotationVO();
        simVO.setWeight(ccNode.getSimilarityWeight());

        //TODO sim function name not available currently
        return simVO;
    }
}
