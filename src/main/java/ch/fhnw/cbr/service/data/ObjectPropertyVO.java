/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service.data;

import ch.fhnw.cbr.core.config.CascadeDelete;

/**
 *
 * @author sandro.emmenegger
 */
public class ObjectPropertyVO extends AbstracVO{
    
    private ClassVO rangeClass;
    private SimilarityAnnotationVO similarityAnnotation;
    private CascadeDelete cascadeDeleteRule = CascadeDelete.ALLWAYS;

    public ClassVO getRangeClass() {
        return rangeClass;
    }

    public void setRangeClass(ClassVO rangeClass) {
        this.rangeClass = rangeClass;
    }
    
    public SimilarityAnnotationVO getSimilarityAnnotation() {
        return similarityAnnotation;
    }

    public void setSimilarityAnnotation(SimilarityAnnotationVO similarityAnnotation) {
        this.similarityAnnotation = similarityAnnotation;
    }

    public CascadeDelete getCascadeDeleteRule() {
        return cascadeDeleteRule;
    }

    public void setCascadeDeleteRule(CascadeDelete cascadeDeleteRule) {
        this.cascadeDeleteRule = cascadeDeleteRule;
    }
    
}
