/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service.data.mapper;

import ch.fhnw.cbr.core.config.CBR;
import ch.fhnw.cbr.model.CCNode;
import ch.fhnw.cbr.service.data.AbstracVO;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntResource;

/**
 *
 * @author sandro.emmenegger
 */
abstract class AbstractVOMapper<F,T> {
    
    public abstract T createVO(F fromObject);
    
    protected void initVO(AbstracVO valueObject, Individual individual) {
        valueObject.setUri(individual.getURI());
        valueObject.setLabel(getLabel(individual));
        valueObject.setTypeUri(individual.getOntClass().getURI());
        valueObject.setTypeLabel(getLabel(individual.getOntClass()));
    }
    
    protected void initVO(AbstracVO valueObject, OntResource ontResource) {
        valueObject.setUri(ontResource.getURI());
        valueObject.setLabel(getLabel(ontResource));
    }    
    
    protected void initVO(AbstracVO valueObject, CCNode ccNode) {
        valueObject.setUri(ccNode.getURI());
        valueObject.setLabel(ccNode.getLabel());
    }    
    
    protected String getLabel(OntResource instance) {
        String label = firstNonNull(instance.getLabel(CBR.LANGUAGE), instance.getLabel(CBR.ALTERNATIVE_LANGUAGE), instance.getLabel(null), instance.getLocalName(), "no label");
        return label;
    }

    protected String firstNonNull(String... paramters) {
        for (String parameter : paramters) {
            if (parameter != null) {
                return parameter;
            }
        }
        return null;
    }
    
    
}
