/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service.data.mapper;

import ch.fhnw.cbr.model.CBRCaseView;
import ch.fhnw.cbr.model.CBRConcern;
import ch.fhnw.cbr.model.CBRRole;
import ch.fhnw.cbr.service.data.CaseViewVO;
import ch.fhnw.cbr.service.data.ConcernVO;
import ch.fhnw.cbr.service.data.RoleVO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sandro.emmenegger
 */
public class CaseViewVOMapper extends AbstractVOMapper<CBRCaseView, CaseViewVO>{

    @Override
    public CaseViewVO createVO(CBRCaseView cbrView) {
        CaseViewVO view = new CaseViewVO();
        initVO(view, cbrView.getIndividual());
        List<ConcernVO> concerns = new ArrayList<>();
        for (CBRConcern cbrConcern : cbrView.getAddressedConcerns()) {
            ConcernVO concern = new ConcernVO();
            initVO(concern, cbrConcern.getIndividual());
            List<RoleVO> roles = new ArrayList<>();
            for (CBRRole cbrRole : cbrConcern.getBelongsToRoles()) {
                RoleVO role = new RoleVO();
                initVO(role, cbrRole.getIndividual());
                roles.add(role);
            }
            concern.setBelongsToRoles(roles);
            concerns.add(concern);
        }
        view.setAddressedConcerns(concerns);
        return view;
    }
    
}
