/* 
 * Copyright 2016 University of Applied Sciences and Arts Northwestern Switzerland FHNW.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.fhnw.cbr.service;

import ch.fhnw.cbr.service.data.CaseInstanceVO;
import ch.fhnw.cbr.service.data.CaseMatchVO;
import ch.fhnw.cbr.service.data.CaseViewVO;
import ch.fhnw.cbr.service.data.ClassVO;
import ch.fhnw.cbr.service.data.IndividualVO;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author andreas.martin
 */
@Remote
public interface CBRServicesInterface {

    void adaptCase(String newCaseInstanceUri, String selectedCaseInstanceUri);

    CaseInstanceVO createOrUpdateCase(CaseViewVO caseView, CaseInstanceVO caseInstanceVO);

    CaseViewVO findCaseViewByUri(String uri);

    List<CaseViewVO> getAllCaseViews();

    List<IndividualVO> getAllIndividuals(String classUri);

    ClassVO getCaseCharacterisationSpace(CaseViewVO caseView);

    CaseInstanceVO getCaseFileItems(String caseInstanceUri);

    CaseInstanceVO getCaseInstance(String instanceUri, CaseViewVO caseView);

    List<CaseMatchVO> retreiveCases(CaseViewVO caseView, CaseInstanceVO querycase);

    boolean setForceRDFSLabelAndComment(boolean forceRDFSLabelAndComment);

    void updateCaseFileItems(String caseInstanceUri, List caseFileItems);
    
}
